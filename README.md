# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* Bash Script Practice

### Write a script that allows the user to search for specific lines in a server access log file according to a pattern and then provide and have these lines written to a new file for further use.
### Details
* Call the script searchlog.sh
* Prompt the user for:  
a) The pattern to be searched for  
b) Whether they want an exact word match or wildcard line match  
c) If they want to do an inverted match, i.e. retrieve lines that do not contain the pattern
*	If no match is found, report to the user “No matches found”. If a match is found, echo the number of matches found, e.g. 5 matches found, and then each of matching word(s)/line(s) to the terminal with their corresponding line numbers in the source file. In either case, ensure the user has the option to do another search if they so wish or or exit the script.
* Ensure all search options are case-insensitive.
* To assist you in writing this script, a file named access_log.txt has been supplied with sample server access data. Please note that your tutor will use a file with the same name and structure as the access_log.txt file provided, but containing different data. Do not therefore hard-code any of the values within the access_log.txt file provided.
* When your tutor marks your script, the access_log.txt file used will be located in the same directory as your script when marked. There is no need therefore to prompt for the location of access_log.txt as part of your script.
