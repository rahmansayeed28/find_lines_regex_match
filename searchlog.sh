
#!/bin/bash

while true; do
    read -p 'pattern you want to search: ' pattern
    read -p 'Choose 1) Exact Word Match /n or /n 2) Wildcard Line Match: ' typematch

    if [ $typematch = 1 ]; then
        read -p 'Choose option: 1)Non Inverted Match or 2) Inverted Match : ' inv

        if [ $inv = 1 ]; then
            count=$(grep -wci $pattern access_log.txt)
            if [ $count -gt 0 ]; then
                echo "$count matches found"
                grep -win $pattern access_log.txt
            else
                echo "No matches found"
            fi
        elif [ $inv = 2 ]; then
            count=$(grep -vci $pattern access_log.txt)
              
            if [ $count -gt 0 ]; then
                echo "$count inverted matches found"
                grep -inv $pattern access_log.txt
            else
                echo "No matches found"
            fi
        else
            echo "you have to choose etiher option 1 or 2"
        fi
        
    elif [ $typematch = 2 ]; then
        read -p 'Choose option: 1)Non Inverted Match or 2) Inverted Match : ' inv
         if [ $inv = 1 ]; then
            count=$(grep -Eci $pattern access_log.txt)
            if [ $count -gt 0 ]; then
                echo "$count matches found"
                grep -Eni $pattern access_log.txt
            else
                echo "No matches found"
            fi
        elif [ $inv = 2 ]; then
            count=$(grep -Evci $pattern access_log.txt)
              
            if [ $count -gt 0 ]; then
                echo "$count inverted matches found"
                grep -Evni $pattern access_log.txt
            else
                echo "No matches found"
            fi
        else
            echo "Option 1 or 2"
        fi
        
    else 
        echo "Wrong option. Start Again"
        continue
    fi

    read -p 'Would you like to search again (yes/no): ' ans
    if [ $ans = "yes" ]; then
        continue
    elif [ $ans = "no" ]; then
        exit 0
    fi 
done
exit 0